package wxpay

import (
	"encoding/json"

	httpcommon "gitee.com/bm_guangge/bm_common/http_common"
)

type AmountData struct {
	Total    int    `json:"total"`              // 订单总金额，单位为分。
	Currency string `json:"currency,omitempty"` // CNY：人民币，境内商户号仅支持人民币。
}
type NativeData struct {
	Appid       string     `json:"appid"`                 // 由微信生成的应用ID，全局唯一。
	Mchid       string     `json:"mchid"`                 // 直连商户的商户号，由微信支付生成并下发。
	Description string     `json:"description"`           // 商品描述
	OutTradeNo  string     `json:"out_trade_no"`          // 商户系统内部订单号，只能是数字、大小写字母_-*且在同一个商户号下唯一
	TimeExpire  string     `json:"time_expire,omitempty"` // 订单失效时间 yyyy-MM-DDTHH:mm:ss+TIMEZONE
	Attach      string     `json:"attach,omitempty"`      // 附加数据，在查询API和支付通知中原样返回，可作为自定义参数使用，实际情况下只有支付完成状态才会返回该字段
	NotifyUrl   string     `json:"notify_url"`            // 知URL必须为直接可访问的URL
	GoodsTag    string     `json:"goods_tag,omitempty"`   // 订单优惠标记
	Amount      AmountData `json:"amount"`                // 订单金额信息
}

type NativeReponse struct {
	CodeUrl string `json:"code_url"`
}

// native下单 https://api.mch.weixin.qq.com/v3/pay/transactions/native
func WxNative(d NativeData) (string, error) {
	res, err := httpcommon.SendHttpPost("https://api.mch.weixin.qq.com/v3/pay/transactions/native", d)
	if err != nil {
		return "", err
	}

	nr := NativeReponse{}
	err = json.Unmarshal([]byte(res), &nr)
	if err != nil {
		return "", err
	}

	return nr.CodeUrl, nil
}
