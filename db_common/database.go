package db_common

import (
	"database/sql"
	"errors"
	// "gitee.com/bm_guangge/registration/service/config"
)

var Gdb *sql.DB

func InitDatabase(dbtype, user, pwd, host, dbname, charset string, port int16, maxOpenConns int) error {
	switch dbtype {
	case "mysql":
		return initMysql(user, pwd, host, dbname, charset, port, maxOpenConns)
	default:
		return errors.New("unsport database type")
	}
}
