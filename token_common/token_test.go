package tokencommon

import (
	"encoding/json"
	"fmt"
	"testing"
	"time"
)

func TestGenToken(t *testing.T) {
	InitToken("test_key")
	tk, err := GenerateToken(5, nil)
	if err != nil {
		t.Errorf("测试失败:%s", err.Error())
	}

	fmt.Println("token:", tk)
	r, d, err := CheckToken(tk)
	if err != nil {
		t.Errorf("测试失败:%s", err.Error())
	}

	if d != nil {
		t.Errorf("测试失败, 数据为空")
	}

	if r != TOKEN_VALID {
		t.Errorf("测试失败，token应该有效: r=%d", r)
	}

	time.Sleep(time.Second * 6)

	r, d, err = CheckToken(tk)
	if err == nil {
		t.Errorf("测试失败:%s", err.Error())
	}

	if d != nil {
		t.Errorf("测试失败, 数据为空")
	}

	if r != TOKEN_EXPIRES {
		t.Errorf("测试失败，token应该过期: r=%d", r)
	}

	type TestData struct {
		Test1 string `json:"test1"`
		Test2 string `json:"test2"`
		Test3 string `json:"test3"`
	}
	tk, err = GenerateToken(5, TestData{Test1: "test1Data", Test2: "test2Data", Test3: "test3Data"})
	if err != nil {
		t.Errorf("测试失败")
	}

	fmt.Println("token:", tk)
	r, d, err = CheckToken(tk)
	if err != nil {
		t.Errorf("测试失败:%s", err.Error())
	}

	if d == nil {
		t.Errorf("测试失败, 数据不为空")
	}
	if r != TOKEN_VALID {
		t.Errorf("测试失败，token应该有效: r=%d", r)
	}

	bs, err := json.Marshal(d)
	if err != nil {
		t.Errorf("测试失败 数据不json")
	}
	td := TestData{}

	err = json.Unmarshal(bs, &td)
	if err != nil {
		t.Errorf("测试失败:%s", err.Error())
	}
	t.Logf("test1:%s,test2:%s,test3:%s", td.Test1, td.Test2, td.Test3)

	time.Sleep(time.Second * 6)

	r, _, err = CheckToken(tk)
	if err == nil {
		t.Errorf("测试失败:%s", err.Error())
	}

	// if d != nil {
	// 	t.Errorf("测试失败, 数据不为空")
	// }

	if r != TOKEN_EXPIRES {
		t.Errorf("测试失败，token应该过期: r=%d", r)
	}
	// t.Logf("test1:%s,test2:%s,test3:%s", td.Test1, td.Test2, td.Test3)

	t.Log("test success")
}
