package reguxar

import "regexp"

func PhoneNumReg(n string) bool {
	// 严谨匹配 /^[1](([3][0-9])|([4][5-9])|([5][0-3,5-9])|([6][5,6])|([7][0-8])|([8][0-9])|([9][1,8,9]))[0-9]{8}$/
	// 不严谨的匹配
	reg := regexp.MustCompile(`^1[3|4|5|6|7|8|9][0-9]{9}$`)
	return reg.MatchString(n)
}

func EmailReg(e string) bool {
	reg := regexp.MustCompile(`^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$`)
	return reg.MatchString(e)
}
