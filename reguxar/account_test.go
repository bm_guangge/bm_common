package reguxar

import "testing"

func Test_PhoneNumReg(t *testing.T) {
	pn := "13456789012"
	if b := PhoneNumReg(pn); !b {
		t.Errorf("测试失败:%s 是正常手机号", pn)
	}

	pn = "12456789012"
	if b := PhoneNumReg(pn); b {
		t.Errorf("测试失败:%s 不是正常手机号", pn)
	}

	pn = "11345678903"
	if b := PhoneNumReg(pn); b {
		t.Errorf("测试失败:%s 不是正常手机号", pn)
	}

	pn = "17665319082"
	if b := PhoneNumReg(pn); !b {
		t.Errorf("测试失败:%s 是正常手机号", pn)
	}

	pn = "1766531908"
	if b := PhoneNumReg(pn); b {
		t.Errorf("测试失败:%s 不是正常手机号", pn)
	}

	pn = "176653190821"
	if b := PhoneNumReg(pn); b {
		t.Errorf("测试失败:%s 不是正常手机号", pn)
	}
	// t.Error("t.Error")
	// t.Fail()
	// t.Error("t.Error")
	//t.FailNow()
	t.Log("test success")
}

func Test_EmailReg(t *testing.T) {
	pn := "1@3.c"
	if b := EmailReg(pn); !b {
		t.Errorf("测试失败:%s 是正常邮箱号", pn)
	}

	pn = "1@a.c"
	if b := EmailReg(pn); !b {
		t.Errorf("测试失败:%s 是正常邮箱号", pn)
	}

	pn = "1@a.1"
	if b := EmailReg(pn); !b {
		t.Errorf("测试失败:%s 是正常邮箱号", pn)
	}

	pn = "12@a.c"
	if b := EmailReg(pn); !b {
		t.Errorf("测试失败:%s 是正常邮箱号", pn)
	}

	pn = "296672810@qq.com"
	if b := EmailReg(pn); !b {
		t.Errorf("测试失败:%s 是正常邮箱号", pn)
	}

	pn = "_@a.c"
	if b := EmailReg(pn); !b {
		t.Errorf("测试失败:%s 是正常邮箱号", pn)
	}

	pn = "123@_.c"
	if b := EmailReg(pn); !b {
		t.Errorf("测试失败:%s 是正常邮箱号", pn)
	}

	pn = "q234_.com"
	if b := EmailReg(pn); b {
		t.Errorf("测试失败:%s 不是正常邮箱号", pn)
	}

	t.Log("test success")
}
