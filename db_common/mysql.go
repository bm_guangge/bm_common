package db_common

import (
	"database/sql"
	"fmt"
	"time"

	// "gitee.com/bm_guangge/registration/service/config"

	lg "gitee.com/bm_guangge/bm_common/log"
	_ "github.com/go-sql-driver/mysql"
)

func initMysql(user, pwd, host, dbname, charset string, port int16, maxOpenConns int) error {
	dbDSN := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=%s", user, pwd, host, port, dbname, charset)

	// 打开连接失败
	db, err := sql.Open("mysql", dbDSN)

	//defer MysqlDb.Close();
	if err != nil {
		lg.ELogger.Println("dbDSN: "+dbDSN, " error", err.Error())
		// fmt.ELogger.Println("dbDSN: "+dbDSN, " error", err.Error())
		return err
	}

	// 最大连接数
	db.SetMaxOpenConns(maxOpenConns)
	// 闲置连接数
	db.SetMaxIdleConns(20)
	// 最大连接周期
	db.SetConnMaxLifetime(100 * time.Second)

	if err = db.Ping(); nil != err {
		lg.ELogger.Println("mysql数据库链接失败: " + err.Error())
		return err
	}
	Gdb = db

	// createMysqlTable()
	return nil
}
