package wxpay

import (
	"bytes"
	"encoding/xml"
	"io/ioutil"
	"net/http"
	"strings"

	uuid "github.com/satori/go.uuid"
)

type CloseOrderReq struct {
	AppId      string `xml:"appid"`
	MchId      string `xml:"mch_id"`
	NonceStr   string `xml:"nonce_str"`
	OutTradeNo string `xml:"out_trade_no"`
	Sign       string `xml:"sign"`
}

func WxCloseOrder(appId string, orderCode string) (WxPayResponse, error) {
	var closeOrderReq CloseOrderReq
	closeOrderReq.AppId = appId
	closeOrderReq.MchId = MchId
	sourceUuid := uuid.NewV4()
	formatUuid := strings.Replace(sourceUuid.String(), "-", "", -1)
	closeOrderReq.NonceStr = formatUuid
	closeOrderReq.OutTradeNo = orderCode

	var mReqMap map[string]interface{}
	mReqMap = make(map[string]interface{}, 0)
	mReqMap["appId"] = closeOrderReq.AppId
	mReqMap["mch_id"] = closeOrderReq.MchId
	mReqMap["nonce_str"] = closeOrderReq.NonceStr
	mReqMap["out_trade_no"] = closeOrderReq.OutTradeNo
	closeOrderReq.Sign = WxPayCalcSign(mReqMap, MchSecret)

	xmlReq, err := xml.Marshal(closeOrderReq)
	if err != nil {
		return WxPayResponse{}, err
	}
	strReq := string(xmlReq)
	strReq = strings.Replace(strReq, "CloseOrderReq", "xml", -1)
	xmlReq = []byte(strReq)

	request, err := http.NewRequest("POST", "https://api.mch.weixin.qq.com/pay/closeorder", bytes.NewReader(xmlReq))
	if err != nil {
		return WxPayResponse{}, err
	}
	request.Header.Set("Accept", "application/xml")
	request.Header.Set("Content-Type", "application/xml;charset=utf-8")

	httpClient := http.Client{}
	response, err := httpClient.Do(request)
	if err != nil {
		return WxPayResponse{}, err
	}

	responseBody, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return WxPayResponse{}, err
	}

	var payResponse WxPayResponse
	err = xml.Unmarshal(responseBody, &payResponse)
	if err != nil {
		return WxPayResponse{}, err
	}

	return payResponse, nil
}
